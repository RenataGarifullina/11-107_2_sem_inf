﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inf107.Trees;

namespace ExpressionTreeTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CalcTest()
        {
            ExpressionTree expTree1 = new ExpressionTree(new BinaryTreeNode<string>("5", 5));
            Assert.AreEqual(expTree1.Calc(), 5);

            BinaryTreeNode<string> head = new BinaryTreeNode<string>("*",1,null);
            head.Right = new BinaryTreeNode<string>("/", 3, head);
            head.Left= new BinaryTreeNode<string>("+", 2, head);
            head.Left.Left = new BinaryTreeNode<string>("/", 4, head.Left);
            head.Left.Right = new BinaryTreeNode<string>("*", 5, head.Left);
            head.Left.Left.Left = new BinaryTreeNode<string>("15", 8, head.Left.Left);
            head.Left.Left.Right = new BinaryTreeNode<string>("3", 9, head.Left.Left);
            head.Left.Right = new BinaryTreeNode<string>("*", 5, head.Left);
            head.Left.Right.Left = new BinaryTreeNode<string>("4", 10, head.Left.Right);
            head.Left.Right.Right = new BinaryTreeNode<string>("2",11, head.Left.Right);
            head.Right.Left = new BinaryTreeNode<string>("-",6, head.Right);
            head.Right.Right = new BinaryTreeNode<string>("3", 7, head.Right);
            head.Right.Left.Left = new BinaryTreeNode<string>("8", 12, head.Right.Left);
            head.Right.Left.Right = new BinaryTreeNode<string>("2", 13, head.Right.Left);
            ExpressionTree expTree = new ExpressionTree(head);
            Assert.AreEqual(expTree.Calc(),26);
        }
    }
}
