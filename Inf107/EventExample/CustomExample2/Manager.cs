﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107.EventExample.CustomExample2
{
    /// <summary>
    /// Руководитель
    /// </summary>
    public class Manager
    {
        private string name;
        public string Name { get { return name; } }

        public Manager(string _name)
        {
            name = _name;
        }
        /// <summary>
        /// Найти замену 
        /// </summary>
        protected void FindReplacement(Worker w, 
            GoVacationParams param)
        {
            //происходит поиск замены
            Console.WriteLine($"Присходит поиск замены " +
                $"менеджером {Name} " +
                $"для сотрудника {w.Name}, уходящего в " +
                $"{param.IsAdm} отпуск" +
                $"в период с {param.From.ToShortDateString()}" +
                $" по {param.To.ToShortDateString()}");
        }
        /// <summary>
        /// Подписка на событие выход в отпуск работника
        /// </summary>
        public void Subscribe(Worker w)
        {
            w.GoVacationDelegate += FindReplacement;
        }
        /// <summary>
        /// Отписка от события выход в отпуск работника
        /// </summary>
        public void Unsubcribe(Worker w)
        {
            w.GoVacationDelegate -= FindReplacement;
        }
    }
}
