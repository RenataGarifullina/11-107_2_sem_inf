﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107.EventExample.MailManagerExample
{
    public class MailManagerRunner
    {
        public void Run()
        {
            var mm = new MailManager();
            mm.SimulateNewMail("Рената", "Михаил", 
                "Констрольная на Codeforces");

            var pager = new Pager();
            pager.Subscribe(mm);
            mm.SimulateNewMail("Рената", "Тагир",
                "Домашка");

            var fax = new Fax();
            fax.Subscribe(mm);
            pager.UnSubscribe(mm);
            mm.SimulateNewMail("Рената", "Егор",
                "Список группы");


        }
    }
}
