﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107.EventExample.MailManagerExample
{
    /// <summary>
    /// Параметры сообщения
    /// </summary>
    internal class NewMailEventArgs: EventArgs
    {
        private string mFrom, mTo, subject;

        public NewMailEventArgs(string mf, string mt, string sbj)
        {
            mFrom = mf;
            mTo = mt;
            subject = sbj;
        }

        /// <summary>
        /// От кого сообщение
        /// </summary>
        public string MailFrom { get { return mFrom; } }
        /// <summary>
        /// Кому сообщение
        /// </summary>
        public string MailTo { get { return mTo; } }
        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get { return subject; } }

    }
}
