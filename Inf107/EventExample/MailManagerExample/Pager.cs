﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107.EventExample.MailManagerExample
{
    /// <summary>
    /// Пейджер - подписчик события новое сообщение MailManager-а
    /// </summary>
    internal sealed class Pager
    {
        /// <summary>
        /// Подписаться на событие новое сообщение MailManager-а
        /// </summary>
        public void Subscribe(MailManager mm)
        {
            mm.NewMail += Show;
        }

        /// <summary>
        /// Отписаться от события новое сообщение MailManager-а
        /// </summary>
        public void UnSubscribe(MailManager mm)
        {
            mm.NewMail -= Show;
        }
        private void Show(object sender, NewMailEventArgs e)
        {
            Console.WriteLine($"Печать сообщения от {e.MailFrom}" +
                $" для {e.MailTo} с темой {e.Subject}");
        }
    }
}
