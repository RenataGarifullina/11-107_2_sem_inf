﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Inf107.EventExample.MailManagerExample
{
    /// <summary>
    /// Менеджер сообщений - источник события
    /// </summary>
    internal class MailManager
    {
        /// <summary>
        /// Событие - новое сообщение 
        /// </summary>
        public event EventHandler<NewMailEventArgs> NewMail;

        /// <summary>
        /// Симуляция события
        /// </summary>
        public void SimulateNewMail(string from, string to, string sbj)
        {
            var args = new NewMailEventArgs(from, to, sbj);
            OnNewMail(args);
        }

        /// <summary>
        /// Обработка события
        /// </summary>
        protected virtual void OnNewMail(NewMailEventArgs args)
        {
            EventHandler<NewMailEventArgs> temp = Volatile.Read(ref NewMail);
            if (temp != null)
                temp.Invoke(this, args);
        }


    }
}
