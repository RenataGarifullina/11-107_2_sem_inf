﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Inf107.Reflection
{
    public class ReflectionRunner
    {
        public void Run() 
        {
            //поработаем с типом Type
            Type t1 = typeof(Int32);
            Type t2 = 5.GetType();
            var i = 5;
            Type t3 = i.GetType();

            Assembly assembly = 
                Assembly.LoadFrom(@"C:\Users\Renata\Desktop\ИТИС\11-107 2 семестр\Программирование\Project\Inf107\LibraryForReflection.dll");

            //получить все типы - 1 способ
            foreach (Type t in assembly.ExportedTypes)
            {
                Console.WriteLine($"FullName:{t.FullName}" +
                    $" Name:{t.Name}");
            }
            //2 способ, более предпочтительный
            var types = assembly.GetTypes();

            //получаем конкретный тип по имени
            var type = assembly.GetType("LibraryForReflection.ClassForReflection");

            //создаем экземпляр с помощью конструктора по умолчанию (без параметров)
            var r1 = System.Activator.CreateInstance(type);

            //вызовем публичный конструктор с целочисленным параметром
            var r2 = 
                System.Activator.CreateInstance(type, new object[] { 55 });

            BindingFlags bd1 = 
                BindingFlags.NonPublic | BindingFlags.Instance;
            //конструктор без параметров
            var r3 = System.Activator.CreateInstance(type, bd1,
                null, new object[] { "paramlamlam" }, null);

            //получим все методы типа
            MethodInfo[] allPublicMethods = type.GetMethods();

            //вызов void DoSmth()
            MethodInfo publicMethod = type.GetMethod("DoSmth");
            publicMethod.Invoke(r3, null);

            //todo вызвать метод DoSmthWithPrm дома

            //вызов int DoSmthWithPrmAndResult(string prm)
            MethodInfo doSmthWithPrmAndResult =
                type.GetMethod("DoSmthWithPrmAndResult");
            object res1 =
                doSmthWithPrmAndResult.Invoke(r1,
                    new object[] { "Renata" });

            //вызов protected int DoSmthWithPrmAndResultProtected(int prm)
            var flags = BindingFlags.NonPublic | BindingFlags.Instance;
            MethodInfo doSmthWithPrmAndResultProtected =
                type.GetMethod("DoSmthWithPrmAndResultProtected", flags);
            object res2 =
                doSmthWithPrmAndResultProtected.Invoke(r1,
                    new object[] { 33 });

            //вызов static void DoStaticMethod()
            flags = BindingFlags.Static | BindingFlags.NonPublic;
            MethodInfo doStaticMethod = type.GetMethod("DoStaticMethod", flags);
            doStaticMethod.Invoke(r1, null);

            //поля
            FieldInfo[] allPublicFields = type.GetFields();
            FieldInfo publicField = type.GetField("PublicField");
            object res3 = publicField.GetValue(r1);
            publicField.SetValue(r1, "New Field Value");
            object res4 = publicField.GetValue(r1);

            //todo самим получить privateField и staticProperty

            //Свойства
            PropertyInfo[] allPublicProperties = type.GetProperties();
            flags = BindingFlags.NonPublic | BindingFlags.Instance;
            PropertyInfo privateProperty =
                type.GetProperty("privateProperty", flags);
            object res5 = privateProperty.GetValue(r1);

            //todo PublicProperty и staticProperty домой

            //события
            EventInfo[] allEvents = type.GetEvents();
            EventInfo testEvent = type.GetEvent("TestEvent");

            //array
            FieldInfo arrayInfo = type.GetField("IntArray");
            object res6 = arrayInfo.GetValue(r1);

            var res7 = Array.CreateInstance(arrayInfo.FieldType, 5);

        }
    }
}
