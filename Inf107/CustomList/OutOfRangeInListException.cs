﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107
{
    public class OutOfRangeInListException: Exception
    {
        private int index;
        private int collectionLength;
        public int Index { get { return index; } }
        public int CollectionLength { get { return index; } }

        public OutOfRangeInListException()
        {
        }

        public OutOfRangeInListException(string message)
            : base(message)
        {
        }

        public OutOfRangeInListException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public OutOfRangeInListException(string message, int index, int collectionLength)
            : base(message)
        {
            this.index = index;
            this.collectionLength = collectionLength;
        }
    }
}
