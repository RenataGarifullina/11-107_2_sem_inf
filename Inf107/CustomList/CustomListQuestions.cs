﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107.CustomList
{
    /// <summary>
    /// Класс для ответов на вопросы
    /// </summary>
    public class CustomListQuestions<T> where T: struct, IComparable<T>
    {
        private Node<T> head;
        public decimal SummaOfAllElements()
        {
            if (head == null)
            {
                return 0;
            }
            if (head.NextNode == null)
            {
                decimal v;
                if (Decimal.TryParse(head.InfField.ToString(), out v))
                    return v;
                else throw new ArgumentException($"Тип {(typeof(T).FullName)} не приводится к численному типу"); ;
            }
            var headCopy = head;
            int result = 0;
            while (headCopy.NextNode != null)
            {
                //todo result += headCopy.InfField;
                headCopy = headCopy.NextNode;
            }
            //todo result += headCopy.InfField;
            return result;
        }
    }
}
