﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107
{
    /// <summary>
    /// Элемент связанного списка
    /// </summary>
    public class Node<T> where T: IComparable<T>
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T InfField;
        /// <summary>
        /// Ссылка на следующий элемент
        /// </summary>
        public Node<T> NextNode;
        /// <summary>
        /// КОнстурктор
        /// </summary>
        public Node(T a) 
        {
            InfField = a;
        }
    }
}
