﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107
{
    /// <summary>
    /// Класс для вызова и проверки функциональности класса CustomList
    /// </summary>
    public class CustomListRunner
    {
        public void Run() 
        {
            try
            {
                var list = new CustomList<int>();
                list.WriteToConsole();
                //list.Add(5, 100);

                var list1 = new CustomList<int>(5);
                list1.WriteToConsole();

                list.Add(1);
                list.WriteToConsole();
                list.Add(2);
                list.WriteToConsole();
                list.Add(3);
                list.WriteToConsole();
                list.AddToHead(0);
                list.WriteToConsole();

                ////list.Insert(1, 5);
                ////list.WriteToConsole();
                ////list.Insert(3, 11);
                ////list.WriteToConsole();

                list.Reverse();
                list.WriteToConsole();

                foreach (var el in list)
                {
                    Console.WriteLine(el.ToString());
                }

            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Добавление на позицию: "
                    + ex.Message + " " + ex.StackTrace);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при выполнении методов " +
                    "линейного списка на основе связного "
                    + ex.Message + " " + ex.StackTrace);
            }
            finally
            {
                //Тут блок finally не нужен, просто тренируемся писать
                Console.WriteLine("Финальные действия выполняются всегда");
            }
        }
    }
}
