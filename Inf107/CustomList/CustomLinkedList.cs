﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107
{
    /// <summary>
    /// Линейный список на основе двунаправленного списка
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomLinkedList<T> : ICustomCollection<T> where T: IComparable<T>
    {
        private LinkedNode<T> head;
        public CustomLinkedList() { }
        public CustomLinkedList(T elem) 
        {
            head = new LinkedNode<T>(elem);
        }
        public CustomLinkedList(T[] array)
        {
            if (array == null || array.Length == 0)
                return;

            //создание головного элемента 
            head = new LinkedNode<T>(array[0]);

            if (array.Length > 1)
            {
                var headCopy = head;
                for (int i = 1; i < array.Length; i++)
                {
                    //создали новый узел (элемент связного списка)
                    var node = new LinkedNode<T>(array[i]);
                    //задаем новому узлу предыдущий элемент
                    //им будет тот элемент, на который смотрит headCopy
                    node.PrevNode = headCopy;
                    //последнему элементу списка присваиваем 
                    //ссылку на следующий элемент, которым
                    //является добавляемый элемент
                    headCopy.NextNode = node;
                    headCopy = headCopy.NextNode;
                }
            }
        }

        public override string ToString() 
        {
            if (head == null)
                return "Список пуст";
            var sb = new StringBuilder();
            var headCopy = head;
            while (headCopy != null)
            {
                sb.Append(headCopy.InfField.ToString());
                if (headCopy.NextNode != null)
                    sb.Append("<=>");
                headCopy = headCopy.NextNode;
            }
            return sb.ToString();
        }

        public void WriteToConsole()
        {
            Console.WriteLine(ToString());
        }

        public void Add(T elem)
        {
            throw new NotImplementedException();
        }

        public void AddRange(T[] elems)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            head = null;
        }

        public bool Contains(T elem)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(T elem)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T elem)
        {
            throw new NotImplementedException();
        }

        public bool isEmpty()
        {
            return head == null;
        }

        public void Remove(T elem)
        {
            throw new NotImplementedException();
        }

        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление с позиции Index
        /// </summary>
        /// <param name="index">с единицы</param>
        public void RemoveAt(int index)
        {
            if (index <= 0)
                throw new Exception("Позиция должна быть больше единицы");

            var size = Size();
            if(head == null || size < index)
                throw new OutOfRangeInListException("Индекс выходит за пределы списка", index, size);

            if (index == 1)
                head = head.NextNode;

            var headCopy = head;
            for (int i = 1; i <= index - 1; i++)
            {
                headCopy = headCopy.NextNode;
            }

            //Если последний элемент
            if (headCopy.NextNode == null)
            {
                headCopy.PrevNode.NextNode = null;
            }
            else 
            {
                //Делаем, что предшествующий удаляемому элемент
                //смотрит на следующий за удаляемым
                headCopy.PrevNode.NextNode =
                    headCopy.NextNode;
                //Делаем, что следующий за удаляемым элемент
                //смотрит на предшествующий удаляемому
                headCopy.NextNode.PrevNode =
                    headCopy.PrevNode;
            }
        }

        public void Reverse()
        {
            throw new NotImplementedException();
        }

        public int Size()
        {
            int length = 0;

            if (head != null)
            {
                var headCopy = head;
                while (headCopy != null)
                {
                    length++;
                    headCopy = headCopy.NextNode;
                }
            }

            return length;
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
