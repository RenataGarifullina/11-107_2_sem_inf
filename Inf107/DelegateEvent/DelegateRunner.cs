﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107
{
    public class DelegateRunner
    {
        //создаем тип делегата, коротый не принимает вх пар-ров
        //и не возвращает результат
        public delegate void Message();

        public delegate int SingleCalc(int x);
        public void Run() 
        {
            //экземпляр делегата
            Message msgHello = WriteHello;
            //способы вызова:
            //1) 
            msgHello();
            //2) через Invoke
            msgHello.Invoke();

            Message msgHello2 = new Message(WriteHello);

            //анонимные методы
            SingleCalc sc =
                delegate (int renata)
                {
                    return renata - 15;
                };
            SingleCalc anonPlusOne = delegate (int x) { return x + 1; };
            anonPlusOne(4);

            //лямбда-выражения
            SingleCalc lamPlusOne = (int x) => { return x + 1; };
            Message lamHello = () => { Console.WriteLine("Hello"); };



        }
        //функция, которая по вх пар-рам и исх знач 
        //соответствует типу делегата Message 
        public void WriteHello() 
        {
            Console.WriteLine("Hello");
        }

    }
}
