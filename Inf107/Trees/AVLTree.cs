﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107.Trees
{
    public class AVLTree
    {
        private BinaryTreeNode<int> root;
        public AVLTree(BinaryTreeNode<int> _root)
        {
            root = _root;
        }

        /// <summary>
        /// Малый левый поворот
        /// </summary>
        /// <param name="r"></param>
        public void SmallLeftTurn(BinaryTreeNode<int> r)
        {
            if (r.Right == null)
                throw new Exception("Малый левый поворот невозможен");
            var newRoot = r.Right;
            r.Right = r.Right.Left;
            newRoot.Left = r;
            r = newRoot;
        }

        /// <summary>
        /// Большой левый поворот
        /// </summary>
        /// <param name="r"></param>
        public void BigLeftTurn(BinaryTreeNode<int> r)
        {
            if (r.Right == null ||
                r.Right.Left == null)
                throw new Exception("Большой левый поворот невозможен");
            var newRoot = r.Right.Left;
            r.Right.Left = newRoot.Right;
            newRoot.Right = r.Right;
            r.Right = newRoot.Left;
            newRoot.Left = r;
            r = newRoot;
        }

        
    }
}
