﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inf107.Trees
{
    public static class TreeUtils<T>
    {
        /// <summary>
        /// Вывод на консоль в ширину
        /// </summary>
        /// <param name="root"></param>
        public static void BreadthFirstSearch(TreeNode<T> root) 
        {
            List<TreeNode<T>> toList = new List<TreeNode<T>>();
            toList.Add(root);
            while(toList.Any())
            {
                var current = toList.First();
                toList.RemoveAt(0);
                if (current.HasChild())
                    toList.AddRange(current.ChildNodeList);
                Console.WriteLine(current.Value.ToString());
            }

        }

        public static int GetHeight(BinaryTreeNode<T> root)
        {
            if (root == null)
                return 0;
            else return 1 + Math.Max(
                GetHeight(root.Left), 
                GetHeight(root.Right));
        }

    }
}
