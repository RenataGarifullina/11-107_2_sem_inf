﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inf107;

namespace Test
{
    [TestClass]
    public class CustomListTest
    {
        [TestMethod]
        public void DeleteAllValuesTest() 
        {
            var clist = new CustomList<int>(
                new int[] { 3, 3, 1, 3, 3, 7, 3, 9, 3});

            clist.RemoveAll(3);

            Assert.AreEqual<int>(3, clist.Size());

            Assert.AreEqual<string>("1 7 9", clist.ToString().Trim());
        }
    }
}
