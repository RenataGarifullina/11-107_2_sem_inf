﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inf107;


namespace Test
{
    [TestClass]
    public class CustomArrayCollectionTest
    {
        [TestMethod]
        public void Test() 
        {
            var ac =
                new CustomArrayCollection<int>(
                    new int[] { 1, 2, 3, 4, 5, 6 });
            ac.RemoveAt(3);
            ac.RemoveAt(0);
            ac.RemoveAt(4);

        }
    }
}
