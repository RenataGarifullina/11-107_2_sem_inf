﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inf107;

namespace Test
{
    [TestClass]
    public class SimpleOperationTest
    {
        [TestMethod]
        public void SquareTest()
        {
            var s = new SimpleOperations();
            Assert.AreEqual(s.Square(2), 4);
            Assert.AreNotEqual(s.Square(3), 5);
        }

        [TestMethod]
        public void DivisionTest() 
        { 
            var s = new SimpleOperations();
            //Assert.ThrowsException<DivideByZeroException>(s.Division(1, 0));
            Assert.AreEqual(0, s.Division(0, 1));
        }
    }
}
